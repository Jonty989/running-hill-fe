import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild('textarea') textarea ;

  constructor(private _http: HttpClient) {}

  wordType: string = "";
  word: string = "";
  selectedWordType: string = "";
  selectedWord: string = ""
  wordString: string = "";

  foundWord: Boolean;

  dbWordOutputs = []

  addWordToDB(){
    if(this.wordType != "" && this.word != ""){
      let body = {
        word: this.word,
        wordType: this.wordType
      }
      this._http.post('http://localhost:3000/word', body).subscribe(result => {
        console.log(result)
      })
    } else {
      console.log("no sorry")
    }
  }

  getWordFromDB(){
    this.dbWordOutputs = [];
    this._http.get<any>('http://localhost:3000/word/' + this.selectedWordType).subscribe(result => {
      result.forEach(res => {
        this.dbWordOutputs.push(res.word)
      })
      if(this.dbWordOutputs.length === 0){
        this.foundWord = false
      } else {

        this.foundWord = true
      }
    })
  }

  deleteWordFromDB(){
    let id = 7
    this._http.delete('http://localhost:3000/word/' + id).subscribe(result => {
      console.log(result)
    })
  }

  useSelectedWord(){
    this.wordString = this.wordString + this.selectedWord + " "
    this.textarea.setFocus()
  }

  submitToBackend(){
    let body = {
      sentence: this.wordString
    }
    this._http.post('http://localhost:3000/sentences', body).subscribe(result => {
      console.log(result)
    })
  }

}
